/* eslint no-param-reassign: ["error", { "props": false }  ] */
export const request = state => {
  state.loading = true;
};
export const success = state => {
  state.loading = false;
  state.errorMsg = '';
};
export const error = (state, msg) => {
  state.loading = false;
  state.errorMsg = msg;
};
