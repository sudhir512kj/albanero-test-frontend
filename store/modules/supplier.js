const state = {};

// user getters
const getters = {};

// user actions
const actions = {
  viewSupplierPendingRequests({ commit }) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      this.$axios
        .$get('supplier/get-pending-requests')
        .then(data => {
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  viewSupplierAcceptedRequests({ commit }, user) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      this.$axios
        .$get('supplier/get-accepted-requests', user)
        .then(data => {
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  acceptRequest({ commit }, requestDetails) {
    return new Promise((resolve, reject) => {
      let info = {
        isAcceptedBySupplier: true,
        supplierAddress: requestDetails.supplierAddress,
        status: 'Accepted By Supplier'
      };
      commit('request', {}, { root: true });
      this.$axios
        .$patch(`supplier/accept-request/${requestDetails.id}`, info)
        .then(data => {
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  }
};

const mutations = {};

export default {
  state,
  getters,
  actions,
  mutations
};
