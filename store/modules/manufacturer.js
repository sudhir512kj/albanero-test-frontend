const state = {
  requestDetails: {}
};

// user getters
const getters = {};

// user actions
const actions = {
  createRequest({ commit }, createRequest) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      console.log(createRequest);
      this.$axios
        .$post('manufacturer/create-request', createRequest)
        .then(data => {
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  viewManufacturerPendingRequests({ commit }, createdBy) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      this.$axios
        .$get('manufacturer/get-pending-requests', { createdBy: createdBy })
        .then(data => {
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  viewManufacturerAcceptedRequests({ commit }, createdBy) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      this.$axios
        .$get('manufacturer/get-accepted-requests', { createdBy: createdBy })
        .then(data => {
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  viewRequest({ commit }, id) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      this.$axios
        .$get(`manufacturer/${id}`)
        .then(data => {
          commit('REQUEST_DETAILS', data);
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  }
};

const mutations = {
  REQUEST_DETAILS: (state, data) => {
    state.requestDetails = data.data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
