const state = {
  token: null,
  status: '',
  userid: '',
  expires_in: '',
  signup_user: {},
  user_info: {}
};

// user getters
const getters = {};

// user actions
const actions = {
  signup({ commit }, user) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      console.log(user);
      this.$axios
        .$post('user/signup/', user)
        .then(data => {
          commit('SIGNUP_SUCCESS');
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  getCoordinates({ commit }, address) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      this.$axios
        .$post('user/get-location', address)
        .then(data => {
          commit('success', {}, { root: true });
          resolve(data);
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  login({ commit }, user) {
    return new Promise((resolve, reject) => {
      commit('request', {}, { root: true });
      this.$auth
        .loginWith('local', {
          data: user,
          transformRequest(obj) {
            const str = [];
            for (const p in obj)
              str.push(
                `${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`
              );
            return str.join('&');
          }
        })
        .then(() => {
          commit('success', {}, { root: true });
          resolve();
        })
        .catch(err => {
          commit('error', err.response.data.error, { root: true });
          reject(err);
        });
    });
  },
  logout({ commit }) {
    return new Promise(resolve => {
      commit('request', {}, { root: true });
      commit('LOGOUT_USER');
      resolve('logout');
    });
  }
};

const mutations = {
  LOGOUT_USER: state => {
    state.token = '';
    // clearing persisted local storage
    setTimeout(() => {
      window.localStorage.removeItem('accessToken');
      window.localStorage.removeItem('app');
      for (const key in localStorage) {
        delete localStorage[key];
      }
    }, 200);

    // clearing cookies
    setTimeout(() => {
      const cookies = document.cookie.split(';');
      for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i];
        const eqPos = cookie.indexOf('=');
        const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT`;
      }
    }, 200);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
