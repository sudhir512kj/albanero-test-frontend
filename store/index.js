import Vuex from 'vuex';
import user from './modules/user';
import manufacturer from './modules/manufacturer';
import supplier from './modules/supplier';

import * as actions from '@/store/actions';
import * as mutations from '@/store/mutations';
import * as getters from '@/store/getters';

const createStore = () =>
  new Vuex.Store({
    state: {
      loading: false,
      email: '',
      errorMsg: '',
      successMsg: ''
    },
    actions,
    mutations,
    getters,
    modules: {
      user,
      manufacturer,
      supplier
    },
    strict: false,
    preserveState: true
  });

export default createStore;
