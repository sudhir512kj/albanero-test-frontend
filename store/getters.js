export const isAuthenticated = state => state.auth.loggedIn;
export const loggedInUser = state => state.auth.user;
