module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'albanero test' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['element-ui/lib/theme-chalk/index.css'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '@/plugins/element-ui', ssr: true },
    '@/plugins/vue2-filters',
    '@/plugins/vue-moment',
    { src: '@/plugins/vue-gravatar', ssr: false },
    '@/plugins/axios',
    { src: '@/plugins/local-storage', ssr: false },
    { src: '@/plugins/vue-local-storage', ssr: false },
    { src: '@/plugins/vue2-google-maps' }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/dotenv',
    '@nuxtjs/component-cache',
    'nuxt-webpackdashboard'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    baseURL: process.env.API_BASE_URL
      ? process.env.API_BASE_URL
      : 'http://localhost:3000/',
    browserBaseURL: process.env.API_BROWSER_BASE_URL
      ? process.env.API_BROWSER_BASE_URL
      : 'http://localhost:3000/'
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: 'user/login',
            method: 'post',
            propertyName: 'data.token'
          },
          user: {
            url: 'user/account/details',
            method: 'get',
            propertyName: false
          },
          logout: false
        }
      }
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    },
    vendor: ['vue2-google-maps'],
    extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    }
  }
};
