# albanero-test-frontend

> albanero test code frontend

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:7000
$ npm run serve

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
